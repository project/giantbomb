<?php

/**
 * User Agent string to use when making making HTTP requests.
 */
const GIANTBOMB_API_UA = 'DrupalGiantBombAPI';

/**
 * GiantBomb API Code: Successfull Query.
 */
const GIANTBOMB_API_CODE_OK = 1;

/**
 * GiantBomb API Code: Invalid API Key.
 */
const GIANTBOMB_API_CODE_INVALID_API_KEY = 100;

/**
 * GiantBomb API Code: Not Found.
 */
const GIANTBOMB_API_CODE_NOT_FOUND = 101;

/**
 * GiantBomb API Code: Invalid URL.
 */
const GIANTBOMB_API_CODE_URL_INVALID = 102;

/**
 * GiantBomb API Code: JSONP Argument Missing.
 */
const GIANTBOMB_API_CODE_JSONP_ARG_MISSING = 103;

/**
 * GiantBomb API Code: Filter Error.
 */
const GIANTBOMB_API_CODE_FILTER_ERROR = 104;

/**
 * GiantBomb API Code: Request Error.
 * 
 * Internal module error, request failed.
 */
const GIANTBOMB_API_CODE_REQUEST_ERROR = 500500;

/**
 * Implements hook_menu().
 */
function giantbomb_api_menu() {
  $items = array();

  $items['admin/config/services/giantbomb'] = array(
    'title' => 'Giant Bomb',
    'description' => 'Giant Bomb configuration.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('giantbomb_api_configuration_form'),
    'access arguments' => array('administer giantbomb_api'),
    'file' => 'giantbomb_api.pages.inc',
  );

  $items['admin/config/services/giantbomb/api'] = array(
    'title' => 'API',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => 0,
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function giantbomb_api_permission() {
  return array(
    'administer giantbomb_api' => array(
      'title' => t('Administer Giant Bomb API'), 
      'description' => t('Grant permission to change Giant Bomb API configuration.'),
    ),
  );
}

/**
 * Get the API key for an endpoint.
 *
 * @param string $endpoint
 *   Endpoint machine name.
 *
 * @return string|bool
 *   An API key, or FALSE
 */
function giantbomb_api_key($endpoint) {
  $keys = variable_get('giantbomb_api_keys', array());
  return isset($keys[$endpoint]) ? $keys[$endpoint] : FALSE;
}

/**
 * Loads a single endpoint.
 *
 * hook_menu() menu wildcard loader compatible.
 *
 * @param string $endpoint
 *   A Giant Bomb endpoint name.
 * 
 * @return array|bool
 *   An endpoint definition, or FALSE.
 */
function giantbomb_api_endpoint_load($endpoint) {
  $endpoints = giantbomb_api_endpoints();
  return isset($endpoints[$endpoint]) ? $endpoints[$endpoint] : FALSE;
}

/**
 * Get all endpoints.
 *
 * @return array
 *   Returns a list of endpoints keyed by endpoint machine name.
 */
function giantbomb_api_endpoints() {
  return array(
    'giantbomb' => array(
      'name' => 'giantbomb',
      'title' => 'Giant Bomb',
      'documentation_uri' => 'http://api.giantbomb.com/documentation/',
      'api_uri' => 'http://api.giantbomb.com/',
    ),
    'comicvine' => array(
      'name' => 'comicvine',
      'title' => 'Comic Vine',
      'documentation_uri' => 'http://api.comicvine.com/documentation/',
      'api_uri' => 'http://api.comicvine.com/',
    ),
  );
}

/**
 * Dictionary of all Giant Bomb response codes
 *
 * @return array
 *   Definition of API response codes keyed by API code.
 */
function giantbomb_api_status_codes() {
  return array(
    GIANTBOMB_API_CODE_OK => t('OK'),
    GIANTBOMB_API_CODE_INVALID_API_KEY => t('Invalid API key'),
    GIANTBOMB_API_CODE_NOT_FOUND => t('Object not found'),
    GIANTBOMB_API_CODE_URL_INVALID => t('Error in URL format'),
    GIANTBOMB_API_CODE_JSONP_ARG_MISSING => t('jsonp format requires a json_callback argument'),
    GIANTBOMB_API_CODE_FILTER_ERROR => t('Filter error'),
    GIANTBOMB_API_CODE_REQUEST_ERROR => t('Request error'),
  );
}

/**
 * Get text description of a status code.
 *
 * @param int $status_code
 *   Pass any GIANTBOMB_API_CODE_* constant.
 *
 * @see giantbomb_api_status_codes().
 */
function giantbomb_api_status($status_code) {
  $status_codes = giantbomb_api_status_codes();
  return isset($status_codes[$status_code]) ? $status_codes[$status_code] : t('Undefined error code'); 
}

/**
 * Raw query request.
 *
 * @param string $path
 *   Example: game/31/
 * @param array $args
 * @param array $query_options
 */
function giantbomb_api_query_request($endpoint, $path, $args = array(), $query_options = array()) {
  $endpoints = giantbomb_api_endpoints();

  $args += array(
    'api_key' => giantbomb_api_key($endpoint),
    'format' => 'json',
  );

  $query_options = $query_options + array(
    'headers' => array(
      'Accept' => 'application/json',
      'User-Agent' => GIANTBOMB_API_UA,
    ),
    'method' => 'GET',
    'data' => '',
  );

  $url = $endpoints[$endpoint]['api_uri'] . $path . '?' . http_build_query($args, '', '&');

  $result = drupal_http_request($url, $query_options);

  if ($result->code == 200 && ($data = json_decode($result->data))) {
    return $data;
  }
  else {
    return FALSE;
  }
}

/**
 * Generic list request.
 * 
 * @param string $endpoint
 * @param string $type
 * @param array $args
 *   (optional) URL arguments from API documentation
 *     - int offset: How many objects into list to start loading. 
 * @param array $options
 *   Valid keys:
 *     - int limit: Limit result counts (to the nearest page). Set to -1 to
 *                  load all pages.
 *
 * @return object
 *   ->status_code: integer
 *   ->results: array
 *
 *   All other properties are optional.
 */
function giantbomb_api_query_list($endpoint, $type, $args = array(), $options = array()) {
  $results = array();
  $pages = 1;
  $path = $type . '/';

  // Load $limit pages. Load all pages if $limit == -1
  $limit = isset($options['limit']) ? $options['limit'] : FALSE;

  $request = giantbomb_api_query_request($endpoint, $path, $args);
  if (isset($request->status_code)) {
    $results = $request->results;
  }
  else {
    $request = new stdClass;
    $request->status_code = GIANTBOMB_API_CODE_REQUEST_ERROR;
    $request->results = array();
  }

  if (!empty($request->number_of_page_results)) {
    $pages = ceil($request->number_of_total_results / $request->number_of_page_results);
  }

  // Optionnally load all pages
  if ($pages > 1 && $limit) {
    for ($i = 1; $i < $pages; $i++) {
      // Current offset
      $args['offset'] = $i * $request->number_of_page_results;

      // Check for limit
      if ($limit != -1 && $args['offset'] >= $limit) {
        break;
      }

      // Query
      $request = giantbomb_api_query_request($endpoint, $path, $args);
      if (isset($request->status_code)) {
        $results = array_merge($results, $request->results);
      }
    }
  }

  $request->results = $results;
  return $request;
}

/**
 * Generic object request.
 * 
 * @param string $endpoint
 * @param string $type
 *   Type
 * @param int $id
 *   ID of object
 * @param array $args
 *   Optional arguments
 * @param array $options
 *
 * @return object
 *   ->status_code: integer
 *   ->results: object
 */
function giantbomb_api_query_object($endpoint, $type, $id, $args = array()) {
  $path = $type . '/' . $id . '/';

  $request = giantbomb_api_query_request($endpoint, $path, $args);
  if (isset($request->status_code)) {
    $results = $request->results;
  }
  else {
    $request = new stdClass;
    $request->status_code = GIANTBOMB_API_CODE_REQUEST_ERROR;
    $request->results = new stdClass;
  }

  return $request;
}

/**
 * Search query.
 *
 * An abstraction to the list query.
 *
 * @param string $endpoint
 * @param string $type
 * @param array $args
 *   (optional) arguments
 * @param array $options
 */
function giantbomb_api_query_search($endpoint, $query, $args = array(), $options = array()) {
  $args['query'] = urlencode($query);

  if (!isset($options['limit'])) {
    // Load all results
    $options['limit'] = -1;
  }

  return giantbomb_api_query_list($endpoint, 'search', $args, $options);
}

/**
 * Get all types.
 *
 * @param string $endpoint
 */
function giantbomb_api_query_types($endpoint) {
  return giantbomb_api_query_list($endpoint, 'types');
}
